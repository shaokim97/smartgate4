#!/usr/bin/python

import smbus
import math
import time
import ulta
import json
import RPi.GPIO as GPIO
 # Power management registers
power_mgmt_1 = 0x6b
power_mgmt_2 = 0x6c
GPIO.setwarnings(False)
#GPIO.setmode(GPIO.BOARD)
GPIO.setup(4,GPIO.OUT, initial=GPIO.HIGH)
GPIO.setup(21,GPIO.OUT, initial=GPIO.LOW)

def read_byte(adr):
    return bus.read_byte_data(address, adr)

def read_word(adr):
    high = bus.read_byte_data(address, adr)
    low = bus.read_byte_data(address, adr+1)
    val = (high << 8) + low
    return val

def read_word_2c(adr):
    val = read_word(adr)
    if (val >= 0x8000):
        return -((65535 - val) + 1)
    else:
        return val

def dist(a,b):
    return math.sqrt((a*a)+(b*b))
    
def get_x_rotation(x,y,z):
    radians = math.atan2(x, dist(y,z))
    return math.degrees(radians)
    
def get_y_rotation(x,y,z):
    radians = math.atan2(y, dist(x,z))
    return -math.degrees(radians)
    
def get_z_rotation(x,y,z):
    radians = math.atan2(z, dist(x,y))
    return -math.degrees(radians)

def get_x_dev(data,status): #status is open or close
    max_value = max(data[status][0]['x'],data[status][1]['x'])
    min_value = min(data[status][0]['x'],data[status][1]['x'])
    dev = round(abs(max_value - min_value) * 10 , 2)
    avr = round((max_value + min_value)/2 , 2)
    data = [dev,avr]
    return data

def get_y_dev(data,status): #status is open or close
    max_value = max(data[status][0]['y'],data[status][1]['y'])
    min_value = min(data[status][0]['y'],data[status][1]['y'])
    dev = round(abs(max_value - min_value) * 10 , 2)
    avr = round((max_value + min_value)/2 , 2)
    data = [dev,avr]
    return data

def get_z_dev(data,status): #status is open or close
    max_value = max(data[status][0]['z'],data[status][1]['z'])
    min_value = min(data[status][0]['z'],data[status][1]['z'])
    dev = round(abs(max_value - min_value) * 10 , 2)
    avr = round((max_value + min_value)/2 , 2)
    data = [dev,avr]
    return data

bus = smbus.SMBus(1) # or bus = smbus.SMBus(1) for Revision 2 boards
address = 0x68       # This is the address value read via the i2cdetect command

# Now wake the 6050 up as it starts in sleep mode
bus.write_byte_data(address, power_mgmt_1, 0)

def gyro():
    while True:
        dist = ulta.distance()
        print ("distance: " , dist)
        time.sleep(0.5)
        if dist > 10:
            print('there is nothing')
            GPIO.output(4, GPIO.LOW)
        elif dist < 10:
            GPIO.output(4, GPIO.HIGH)
            gyro_xout = read_word_2c(0x43)
            gyro_yout = read_word_2c(0x45)
            gyro_zout = read_word_2c(0x47)

            accel_xout = read_word_2c(0x3b)
            accel_yout = read_word_2c(0x3d)
            accel_zout = read_word_2c(0x3f)

            accel_xout_scaled = accel_xout / 16384.0
            accel_yout_scaled = accel_yout / 16384.0
            accel_zout_scaled = accel_zout / 16384.0
            x_rot = round(get_x_rotation(accel_xout_scaled, accel_yout_scaled, accel_zout_scaled),2)
            y_rot = round(get_y_rotation(accel_xout_scaled, accel_yout_scaled, accel_zout_scaled),2)
            z_rot = round(get_z_rotation(accel_xout_scaled, accel_yout_scaled, accel_zout_scaled),2)
            data = [x_rot,y_rot,z_rot]
            time.sleep(2)
            return (data)

if __name__ == '__main__':
    angle = []
    data = {}
    data['close'] = []
    data['open'] = []
    try:
        for i in range(4):
            gate_data = gyro()
            angle = angle + gate_data
            print (angle)
            print(type(gate_data))
            if i % 2 == 0:
#                data.append({
                data['close'].append({
                    'x': gate_data[0],
                    'y': gate_data[1],
                    'z': gate_data[2]
                    })
            elif i % 2 == 1:
#                data.append({
                data['open'].append({
                    'x': gate_data[0],
                    'y': gate_data[1],
                    'z': gate_data[2]
                    })
            time.sleep(0.5)
        x_open_dev = get_x_dev(data,'open')
        y_open_dev = get_y_dev(data,'open')
        z_open_dev = get_z_dev(data,'open')
        x_close_dev = get_x_dev(data,'close')
        y_close_dev = get_y_dev(data,'close')
        z_close_dev = get_z_dev(data,'close')
        data['open'].append({
            'x_dev': x_open_dev[0],
            'y_dev': y_open_dev[0],
            'z_dev': z_open_dev[0],
            'x_avr': x_open_dev[1],
            'y_avr': y_open_dev[1],
            'z_avr': z_open_dev[1]
            })
        data['close'].append({
            'x_dev': x_close_dev[0],
            'y_dev': y_close_dev[0],
            'z_dev': z_close_dev[0],
            'x_avr': x_close_dev[1],
            'y_avr': y_close_dev[1],
            'z_avr': z_close_dev[1]
            })
        
        with open('your_json.ini', 'w') as f:
            json.dump(data,f)
            GPIO.output(4, GPIO.LOW)
            GPIO.output(21, GPIO.HIGH)
#            for item in angle:
#                f.write("%s\n" % item)

        # Reset by pressing CTRL + C
    except KeyboardInterrupt:
        print("Measurement stopped by User")
        GPIO.cleanup()
