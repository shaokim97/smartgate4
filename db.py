import sqlite3

conn = sqlite3.connect('test.db')
print ("Opened database successfully")

conn.execute('''CREATE TABLE DATA
         (ID INT PRIMARY KEY    NOT NULL,
         CODE           TEXT    NOT NULL,
         API            TEXT    NOT NULL,
         FROM           TEXT    NOT NULL,
         TO             TEXT    NOT NULL,
         PASSWORD       TEXT    NOT NULL);''')

print ("Table created successfully")

conn.execute("INSERT INTO DATA (ID,CODE,API,FROM,TO,PASSWORD) \
      VALUES (5775634772, 'BD-0093', 'http://cloudpark.my/API/DeviceEvent', 'raspberrypi.cloudpark@gmail.com', 'cloudpark.rpi@gmail.com', 'cvhnfreslastdrbz' )")

conn.commit()
print ("Records created successfully")
conn.close()