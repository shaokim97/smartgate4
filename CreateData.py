import hashlib
import uuid
import base64
import random
import string
import json
from datetime import datetime, date, timedelta
from subprocess import check_output
import pygame.camera
from PIL import Image, ImageDraw, ImageFont
from gpiozero import CPUTemperature

# 1
# BD-0087 7164357765
# BD-0088 1636054576
# BD-0091 6077475562

# 2
# BD-0112 6652310117
# BD-0116 4700616052
# BD-0119 5104135447
# 3
# BD-0125 0504046464
# BD-0002 1643524316

# DeviceID = "BD-0088"
# DeviceCode = "1636054576"
# Url = "cloudpark.my"
# Endpoint = "/api/DeviceEvent"
# Vid = "Python-RPi3B+"

f = open("device.txt", "r")
contents = f.read().splitlines()
deviceCode = contents[0]
deviceId = contents[1]
IPAddress = check_output(['hostname', '-I'])
IPAddr = IPAddress.decode("utf-8")
cpu = CPUTemperature()


class CreateData:
    def __init__(self):
        f = open("device.txt", "r")
        contents = f.read().splitlines()
        deviceCode = contents[0]
        deviceId = contents[1]
        IPAddress = check_output(['hostname', '-I'])
        IPAddr = IPAddress.decode("utf-8")
        cpu = CPUTemperature()

    def CaptureImage(self):
        try:
            pygame.init()
            pygame.camera.init()
            cam = pygame.camera.Camera("/dev/video0", (640, 480))
            cam.start()
            image = cam.get_image()
            pygame.image.save(image,'1.jpg')
            print('Image Written')
            cam.stop()
        except SystemError:
            img = Image.new('RGB', (150, 50), color='black')
            fnt = ImageFont.truetype('CharlesWright-Bold.ttf', 25)
            d = ImageDraw.Draw(img)
            d.text((2, 2), "Camera down", font=fnt, fill=(255, 255, 255))
            img.save('1.jpg')

    def CreateContent(self, Counter, Cond):
        time = datetime.utcnow()
        timestamp = time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        # Build json content
        GuID = uuid.uuid4().hex
        if Cond == 0:
            self.CaptureImage()
            with open('1.jpg', "rb")as imageFile:
                imgstr = base64.b64encode(imageFile.read())
            json_data = {"transactionId": GuID,
                         "timestamp": timestamp,
                         "eventType": "raised",
                         "vehicleId": "{} Count:{} {}".format(IPAddr, Counter, int(cpu.temperature)),
                         "pictureData": imgstr.decode("utf-8")}
        elif Cond == 1:
            json_data = {"transactionId": GuID,
                         "timestamp": timestamp,
                         "eventType": "Alive",
                         "vehicleId": "{} Temp:{}".format(IPAddr, int(cpu.temperature)),
                         "pictureData": None}
        Content = json.dumps(json_data)
        return Content

    def CreateHeader(self):
        time = datetime.utcnow()
        d = time.strftime("%Y%m%d%H%M")
        str = deviceCode + d
        bytesForHash = bytes(str, 'utf-8')
        hash_object = hashlib.sha256(bytesForHash)
        hex_dig = hash_object.hexdigest().upper()
        AuthToken = hex_dig + deviceId.replace('-', '')

        # Create headers
        Headers = {"Content-Type": "application/json", 'X-AUTH-TOKEN': AuthToken}
        return Headers


if __name__ == "__main__":
    c = CreateData()
    c.CreateContent(1, 0)
