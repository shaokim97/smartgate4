from Gyro import *
from Connection import *
from sendEmail import *
from ClassThread import *
from time import sleep
import logging
from logging import Formatter, StreamHandler
import threading
from threading import Lock
import time
import queue
from logging.handlers import RotatingFileHandler
import psutil


count = 0
sendCount = 0
carsQ = queue.Queue()
lock = Lock()
d = CreateData()
g = Gyro(0x68)
c = Connection()
e = sendEmail()
# logging.basicConfig(filename='info.log', level= logging.INFO, format='%(asctime)s, %(levelname)s:%(message)s',)


root = logging.getLogger()
root.setLevel(logging.INFO)
# log_formatter  = Formatter("%(asctime)s [%(levelname)s] %(name)s: %(message)s [%(threadName)s] ") # I am printing thread id here
log_formatter =Formatter("%(asctime)s, [%(threadName)s]: %(message)s")
console_handler = StreamHandler()
console_handler.setFormatter(log_formatter)
#file_handler = logging.FileHandler(_zone + '.log')
file_handler = RotatingFileHandler(
    'info.log', mode='a', maxBytes=5*1024*1024, backupCount=2)
file_handler.setFormatter(log_formatter)
root.addHandler(console_handler)
root.addHandler(file_handler)

def detectCar():
    carCount = 0
    logging.info("\nThread detectCar() starting")
    state = 0
    while True:
        # Read Accelerometer raw value
        rotation = g.gyro_var()
        # Read Gyroscope raw value
        status = g.checkOpen(rotation)
        print(status)
        if status and state == 0:
            lock.acquire()
            carCount = carCount + 1
            carsQ.put(d.CreateContent(carCount, 0))
            lock.release()
            state = 1
        elif not status and state == 1:
            logging.info("Thread detectCar() " + str(carsQ.qsize()) + " cars detected added to queue.")
            state = 0
            # logging.info("Thread detectCar() sleep")
        else:
            pass

def checkQueueAndSend():
    sendCount = 1
    while True:
        sleep(30)
        while not carsQ.empty():
            data = carsQ.queue[0]
            if c.sendData(data) == 0:
                lock.acquire()
                carsQ.get()
                carsQ.task_done()
                lock.release()
            else:
                break
            logging.info("Thread checkQueue() send data %d", sendCount)
            sendCount = sendCount + 1


def aliveCheck():
    while True:
        c.sendData(d.CreateContent(0, 1))
        logging.info("Alive check")
        sleep(1800)


#if __name__ == "__main__":
    # root = logging.getLogger()
    # root.setLevel(logging.INFO)
    # x = threading.Thread(target=detectCar, args=())
    # x.start()
    # y = threading.Thread(target=checkQueueAndSend, args=())
    # y.start()
    # z = threading.Thread(target=aliveCheck, args=())
    # z.start()
    # w = threading.Thread(target=e.checkLog, args=())
    # w.start()

def main():
    threadStart = False
    while True:
        if threadStart == False:
            csThread = ThreadManager(target=checkQueueAndSend, args=(), daemon=False)
            csThread.start()
            logging.info("csThread was started...")
            entryThread = ThreadManager(target=detectCar, args=(), daemon=False)
            entryThread.start()
            logging.info("entryThread was started...")
            exitThread = ThreadManager(target=aliveCheck, args=(), daemon=False)
            exitThread.start()
            logging.info("exitThread was started...")
            threadStart = True
            # free up resources
            zn = None
            botDataEntry = None
            botDataExit = None
            bot = None

        if threadStart == True:
            if not csThread.is_alive():
                csThread.kill()
                logging.info("csThread was killed...")
                threadStart = False
            if not entryThread.is_alive():
                entryThread.kill()
                logging.info("entryThread was killed...")
                threadStart = False
            if not exitThread.is_alive():
                exitThread.kill()
                logging.info("exitThread was killed...")
                threadStart = False
        tstr = ('s:alive;' if csThread.is_alive() else 's:dead;') + \
               ('e:alive;' if entryThread.is_alive() else 'e:dead;') + \
               ('x:alive;' if exitThread.is_alive() else 'x:dead;')
        logging.info(tstr)

        if psutil.LINUX:
            p = psutil.Process()
            logging.info(psutil.sensors_temperatures())
            logging.info(p.memory_full_info())
        sleep(60)


if __name__ == "__main__":
    main()
