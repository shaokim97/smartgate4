
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from CreateData import *
import time
from time import sleep
import logging

API = "http://cloudpark.my/api/DeviceEvent"
t = CreateData()

class Connection:
	def requests_retry_session(self, retries=3, backoff_factor=0.3, status_forcelist=(500, 502, 504), session=None,):
		session = session or requests.Session()
		retry = Retry(
			total=retries,
			read=retries,
			connect=retries,
			backoff_factor=backoff_factor,
			status_forcelist=status_forcelist,
			)
		adapter = HTTPAdapter(max_retries=retry)
		session.mount('http://', adapter)
		session.mount('https://', adapter)
		return session

	def sendData(self, Content):
		y = json.loads(Content)
		try:
			print("first try send " + y['vehicleId'])
			h = t.CreateHeader()
			r = requests.post(url=API, data=Content, headers=h, timeout=3)
			respond = r.text
			if r.status_code == 200:
				logging.info(str(r.status_code))
			else:
				raise requests.HTTPError(str(r.status_code) + ":" + r.text)
			print(respond)
		except requests.exceptions.Timeout:
			logging.info("Exceptions.Timeout")
			return 1
		except requests.exceptions.TooManyRedirects:
            # Tell the user their URL was bad and try a different one
			logging.info("Exceptions.TooManyRedirects")
			return 2
		except requests.exceptions.RequestException as err:
            # catastrophic error. bail.
			logging.info("Exceptions.RequestException", err)
			return 3
		return 0

