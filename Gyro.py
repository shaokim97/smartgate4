import smbus
import time
import math
import json


gate_data = []


class Gyro:
    def __init__(self, addr):
        self.power_mgmt_1 = 0x6b
        self.power_mgmt_2 = 0x6c
        self.timeout = time.time() + 1
        self.bus = smbus.SMBus(1)  # or bus = smbus.SMBus(1) for Revision 2 boards
        # Now wake the 6050 up as it starts in sleep mode
        self.bus.write_byte_data(addr, self.power_mgmt_1, 0)
        self.addr = addr
        self.readConfig()
        
    def read_word(self, adr):
        high = self.bus.read_byte_data(self.addr, adr)
        low = self.bus.read_byte_data(self.addr, adr + 1)
        val = (high << 8) + low
        return val

    def read_word_2c(self, adr):
        val = self.read_word(adr)
        if (val >= 0x8000):
            return -((65535 - val) + 1)
        else:
            return val

    def gyro_var(self):
        try:
            time.sleep(2)
            accel_xout = self.read_word_2c(0x3b)
            accel_yout = self.read_word_2c(0x3d)
            accel_zout = self.read_word_2c(0x3f)

            accel_xout_scaled = accel_xout / 16384.0
            accel_yout_scaled = accel_yout / 16384.0
            accel_zout_scaled = accel_zout / 16384.0

            rotx = round(self.get_x_rotation(accel_xout_scaled, accel_yout_scaled, accel_zout_scaled), 2)
            roty = round(self.get_y_rotation(accel_xout_scaled, accel_yout_scaled, accel_zout_scaled), 2)
            rotz = round(self.get_z_rotation(accel_xout_scaled, accel_yout_scaled, accel_zout_scaled), 2)
            rotate = [rotx, roty, rotz]
            return rotate
        except OSError:
            print ("OS Error")
            

    def checkClose(self,rotate):
        try:
            if ((gate_data[0] < rotate[0] < gate_data[3]) and (gate_data[1] < rotate[1] < gate_data[4]) and (
                    gate_data[2] < rotate[2] < gate_data[5])):
                return True
            else:
                return False
        except TypeError:
            pass

    def checkOpen(self,rotate):
        try:
            if ((gate_data[6] < rotate [0]< gate_data[9]) and (gate_data[7] < rotate[1] < gate_data[10]) and (
                gate_data[8] < rotate[2] < gate_data[11])):
                return True
            else:
                return False
        except TypeError:
            pass

    def readConfig(self):
        gate0 = [0]*6
        gate1 = [0]*6
        
        with open('your_json.ini') as json_data:
            data = json.load(json_data)
            for p in data['close'][-1:]:
                x_close_dev = p['x_dev']
                y_close_dev = p['y_dev']
                z_close_dev = p['z_dev']
                x_close_avr = p['x_avr']
                y_close_avr = p['y_avr']
                z_close_avr = p['z_avr']
            for d in data['open'][-1:]:
                x_open_dev = d['x_dev']
                y_open_dev = d['y_dev']
                z_open_dev = d['z_dev']
                x_open_avr = d['x_avr']
                y_open_avr = d['y_avr']
                z_open_avr = d['z_avr']
        gate0[0] = x_close_avr - x_close_dev  # gate0 contains the gyro data during gate closed
        gate0[1] = y_close_avr - y_close_dev
        gate0[2] = z_close_avr - z_close_dev
        gate0[3] = x_close_avr + x_close_dev
        gate0[4] = y_close_avr + y_close_dev
        gate0[5] = z_close_avr + z_close_dev
        gate1[0] = x_open_avr - x_open_dev  # gate1 contains the gyro data during gate open
        gate1[1] = y_open_avr - y_open_dev
        gate1[2] = z_open_avr - z_open_dev
        gate1[3] = x_open_avr + x_open_dev
        gate1[4] = y_open_avr + y_open_dev
        gate1[5] = z_open_avr + z_open_dev
        self.gate_data = gate0 + gate1  # gate_data[0:5]contains x,y,z of gate close, [6:11] contains gate open
        global gate_data
        gate_data = gate0 + gate1
        return gate_data

    @staticmethod
    def dist(a, b):
        return math.sqrt((a * a) + (b * b))

    def get_x_rotation(self, x, y, z):
        radians = math.atan2(x, self.dist(y, z))
        return math.degrees(radians)

    def get_y_rotation(self, x, y, z):
        radians = math.atan2(y, self.dist(x, z))
        return -math.degrees(radians)

    def get_z_rotation(self, x, y, z):
        radians = math.atan2(z, self.dist(x, y))
        return -math.degrees(radians)
    
#g = Gyro(0x68)
#
#while True:
#    rot = g.gyro_var()
#    print(rot)
#    status = g.checkOpen(rot)
#    print(status)