import smtplib
from email.message import EmailMessage
import socket
import fcntl
import struct
import os
from time import sleep


class sendEmail():
	def __init__(self):
		self.retryCount = 0

	def sendMail(self):
		filesize = os.path.getsize('info.log')
		EMAIL_ADDRESS = 'raspberrypi.cloudpark@gmail.com'
		EMAIL_PASSWORD = 'cvhnfreslastdrbz'

		msg = EmailMessage()
		msg['Subject'] = 'Testing Log'
		msg['From'] = EMAIL_ADDRESS
		msg['To'] = 'cloudpark.rpi@gmail.com'
		msg.set_content('file size = {}'.format(filesize))

		with open('info.log', 'rb') as f:
			file_data = f.read()
			file_name = f.name

		msg.add_attachment(file_data, maintype='text', subtype='log', filename=file_name)

		with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:
			smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
			smtp.send_message(msg)

		smtp.quit()
		open('info.log', 'w').close()
		time.sleep(1)
		logging.getLogger().addHandler(filehandler)

	def checkLog(self):
		max_size = 1000000
		while True:
			sleep(3600)
			file_size = os.path.getsize('info.log')
			if file_size > max_size:
				try:
					self.sendMail()
				except smtplib.SMTPConnectError:
					if self.retryCount < 3:
						logging.info("Retrying to send email in 5 seconds, Retry count: {}".format(self.retryCount))
						self.sendMail()
						sleep(5)
						self.retryCount += 1
				self.retryCount = 0
			else:
				return
