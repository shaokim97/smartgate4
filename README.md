Steps
1. Update Device code, Id in device.txt
2. Run the deviceConfig.py to get the angle of the gate
	2.1 Before running deviceConfig.py, make sure ultra sonic sensor and gyro sensor are installed correctly
	2.2 Close the gate and place something infront of the ultrasonic sensor
	2.3 Open the gate and place something infront of the ultrasonic sensor
	2.4 Repeat step 2.2 and 2.3 again
	2.5 Then you will have a log file called "your_json.ini" 
3. Run the main.py